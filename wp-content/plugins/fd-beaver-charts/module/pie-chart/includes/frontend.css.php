/**
* This file should contain frontend styles that
* will be applied to individual module instances.
*
* You have access to three variables in this file:
*
* $module An instance of your module class.
* $id The module's ID.
* $settings The module's settings.
*
*
*/

.fl-node-<?php echo esc_attr($id); ?> #pie-chart-<?php echo esc_attr($id); ?> {
    <?php if (!empty($settings->chart_size)): ?>
        height: <?php echo intval($settings->chart_size); ?>px;
    <?php endif; ?>

    <?php if (!empty($settings->chart_size)): ?>
        width: <?php echo intval($settings->chart_size); ?>px;
    <?php endif; ?>
}
