<?php
/**
 * This file should be used to render each module instance.
 * $settings The module's settings.
 */
?>
<div class="bar-chart-container">
    <div class="bar-chart-content">
        <canvas id="fl-barchart-<?php echo esc_attr($id); ?>"></canvas>
    </div>
</div>